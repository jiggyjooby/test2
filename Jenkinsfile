pipeline {
    agent {
        label 'windows-shared'
    }

    environment { 
        PROJECT_NAME 				= 'test-project'
        PROJECT_S3_BUCKET_REGION 		= 'us-east-1'
        PROJECT_S3_BUCKET_NAME 			= 'grammable-travis-watson'
       //PROJECT_BUILD_OUTPUT_FILE_NAME 		= 'test.zip'              
        //PROJECT_SOLUTION_NAME 			= "HelloWorld.sln"
    }

    stages {
        stage('Cloning the project repository from Github') {
            steps {
                checkout(
                    [
                        $class: 'GitSCM', 
                        branches: [[name: '*/master']], 
                        doGenerateSubmoduleConfigurations: false, 
                        extensions: [[$class: 'CleanBeforeCheckout']], 
                        submoduleCfg: [], 
                        userRemoteConfigs: [
                            [
                                credentialsId: 'git', 
				                url: "$repoUrl"
                            ]
                        ]
                    ]
                )
            }
        }
	    
	stage('Restore packages'){
		steps {
			script{
                //bat "cd ${workspace}\\HelloWorld\\"
				bat "C:\\ProgramData\\chocolatey\\lib\\NuGet.CommandLine\\tools\\nuget.exe restore ${solutionName}.sln"
			}
		}
	}

        stage('Building the project') {
            steps {
		    script{
			def msbuild = tool name: 'MSBuild', type: 'hudson.plugins.msbuild.MsBuildInstallation'
         		bat "\"${msbuild}\" ${solutionName}.sln  /p:DeployOnBuild=true /p:DeployDefaultTarget=WebPublish /p:WebPublishMethod=FileSystem /p:SkipInvalidConfigurations=true /t:build /p:Configuration=Release /p:Platform=\"Any CPU\" /p:DeleteExistingFiles=True /p:publishUrl=$WORKSPACE\\zip\\$zipFileName" 
		    }
	    }
       }
       
         stage('replace web config') {
             steps {
                 withCredentials([[
                 $class: 'AmazonWebServicesCredentialsBinding',
                 accessKeyVariable: 'AWS_ACCESS_KEY_ID', // dev credentials
                 credentialsId: 'AWSCRED',
                 secretKeyVariable: 'AWS_SECRET_ACCESS_KEY'
                 ]]){
                     powershell '''
                         Import-Module AWSPowerShell
                         Set-AWSCredentials -AccessKey "$($ENV:AWS_ACCESS_KEY_ID)" -SecretKey "$($ENV:AWS_SECRET_ACCESS_KEY)" -StoreAs "$($ENV:PROJECT_NAME)"
                         Read-S3Object -BucketName "$($ENV:PROJECT_S3_BUCKET_NAME)" -Key "/$($ENV:zipFileName)/dev/Web.config" -File "$($ENV:WORKSPACE)/zip/$($ENV:zipFileName)/Web.config"
                     '''
                 }
             }
         }

	    
        stage ('zip artifact') {
            steps {
                script{ zip zipFile: "${zipFileName}.zip", archive: false, dir: "zip\\$zipFileName" }
		archiveArtifacts artifacts: "${zipFileName}.zip", fingerprint: true
            }
        }
        

         stage('Uploading application package to s3') {
             steps {
                 withCredentials([[
                 $class: 'AmazonWebServicesCredentialsBinding',
                 accessKeyVariable: 'AWS_ACCESS_KEY_ID', // dev credentials
                 credentialsId: 'AWSCRED',
                 secretKeyVariable: 'AWS_SECRET_ACCESS_KEY'
                 ]]){
                     powershell '''
                         Import-Module AWSPowerShell
                         Set-AWSCredentials -AccessKey "$($ENV:AWS_ACCESS_KEY_ID)" -SecretKey "$($ENV:AWS_SECRET_ACCESS_KEY)" -StoreAs "$($ENV:PROJECT_NAME)"
                         Write-S3Object -BucketName "$($ENV:PROJECT_S3_BUCKET_NAME)" -File "$($ENV:zipFileName).zip" -Region "$($ENV:PROJECT_S3_BUCKET_REGION)" -ProfileName "$($ENV:PROJECT_NAME)"
                     '''
                 }
             }
         }
    }
}
